﻿using GuessNumber.Bot;
using System;
using System.Reflection;
using System.Linq;
using System.Collections.Generic;
using System.Threading;

namespace GuessNumber
{
    class Program
    {
        const string MASKOK = "4A0B";
        const int MAX_GUESS_COUNT = 50;
        const int PK_DEFAULT_ROUNDS = 1000;
        const int PK_PRINTING_LIMIT = 20;
        static List<TypeInfo> knownBotTypes;

        static void Main(string[] args)
        {
            Console.WindowWidth = 120;
            Console.WindowHeight = 40;

            while (true)
            {
                string h1 = "Welcome to GuessNumber";
                Console.Clear();
                Console.SetCursorPosition((Console.WindowWidth - h1.Length) / 2 - 2, 3);
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine(h1);
                Console.ResetColor();
                Console.SetCursorPosition(0, 7);

                Console.WriteLine("Please select a mode：");
                Console.WriteLine("1. Human Play");
                Console.WriteLine("2. Bot Vs Bot");
                Console.WriteLine();

                string mode = Console.ReadLine();
                if (mode == "1")
                {
                    ShowGameHelp();

                    StartGame();
                }
                else if (mode == "2")
                {
                    StartPK();
                }
            }
        }

        private static void StartGame()
        {
            _Foo bot = new _Foo();

            bot.Init();

            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Please enter a four-digit number(the first digit could be zero):");
            Console.ResetColor();

            int guessCount = 0;
            int guessNum;

            string inputAnswer;
            string judgeResult = string.Empty;
            while (true)
            {
                inputAnswer = Console.ReadLine().Trim();
                if (inputAnswer == "?" || inputAnswer == "？")
                {
                    Console.WriteLine();
                    Console.WriteLine("The answer is {0:D4}", bot.Answer);
                    break;
                }
                else if (inputAnswer.Length == 4 && int.TryParse(inputAnswer, out guessNum))
                {
                    try
                    {
                        judgeResult = bot.Judge(guessNum);

                        Console.Write("[{0}] You got", ++guessCount);
                        Console.ForegroundColor = ConsoleColor.Yellow;
                        Console.Write(" {0} ", judgeResult);
                        Console.ResetColor();
                        Console.Write("according to your answer");
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.Write(" {0} ", inputAnswer);
                        Console.ResetColor();
                        Console.Write(", ");

                        if (judgeResult == "4A0B")
                        {
                            Console.WriteLine("Congratulations!\r\n");
                            break;
                        }
                        else
                        {
                            Console.Write("Sorry, not match, please try again:");
                        }
                    }
                    catch (BizException)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.Write("Input error, please try again:");
                        Console.ResetColor();
                    }
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Write("Input error, please try again:");
                    Console.ResetColor();
                }
            }

            Console.WriteLine("Press any key to start next round or press Backspace to get the main menu...");
            if (Console.ReadKey().Key != ConsoleKey.Backspace)
            {
                StartGame();
            }
        }

        private static void ShowGameHelp()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(" - 系统将生成一个4位数字，这个数字首位可以为零，但各位不相同，由您来猜测系统生成的数字到底是多少。");
            Console.WriteLine();
            Console.WriteLine(" - 在您输入猜测的答案之后，系统会按照个、十、百、千每一位进行比较，并以 ?A?B 的形式返回结果。其中:");
            Console.WriteLine(" \t- A 表示您猜测的数字大小和所在位置完全正确的个数；");
            Console.WriteLine(" \t- B 表示您猜测的数字存在于答案的4位数中，但位置错误。");
            Console.WriteLine();
            Console.WriteLine(" - 比如：系统生成了5028，而您猜测输入的是0178，则系统会返回 1A1B");
            Console.WriteLine(" \t- 这表示您猜测的数字有1位完全正确（8），");
            Console.WriteLine(" \t- 而有1位虽然答案数字里有，但位置错误（0）");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine();
            Console.WriteLine(" - 游戏过程中输入“？”将会给出答案并转到下一局游戏。");
            Console.WriteLine();
            Console.ResetColor();
        }

        private static void StartPK()
        {
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Please enter the number of the two bots, separated by a space:");
            Console.WriteLine();
            for (int i = 0; i < knownBotTypes.Count; i++)
            {
                Console.WriteLine("[{0}] {1}", i, knownBotTypes[i]);
            }
            Console.ResetColor();
            Console.WriteLine();

            bool isOk = false;
            var playersStr = Console.ReadLine().Trim();
            int[] ids = new int[2];
            if (!string.IsNullOrEmpty(playersStr))
            {
                var playerIDs = playersStr.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                if (playerIDs.Length == 2)
                {
                    int tmp;
                    if (int.TryParse(playerIDs[0], out tmp))
                    {
                        ids[0] = tmp;

                        if (int.TryParse(playerIDs[1], out tmp))
                        {
                            ids[1] = tmp;

                            if (ids.All((x) => { return x < knownBotTypes.Count; }))
                            {
                                isOk = true;
                            }
                        }
                    }
                }
            }

            if (!isOk)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Input Error!");
                Console.ResetColor();
            }
            else
            {
                Console.Write("Please enter the PK rounds, [ENTER] for {0} by default: ", PK_DEFAULT_ROUNDS);

                int pkCount = PK_DEFAULT_ROUNDS;
                int tmp;
                if (int.TryParse(Console.ReadLine(), out tmp) && tmp > 0)
                {
                    pkCount = tmp;
                }

                Console.WriteLine("PK Begin...");

                int round = 0;
                var winsA = new List<long[]>();
                var winsB = new List<long[]>();
                int draws = 0;

                while (round++ < pkCount)
                {
                    if (pkCount <= PK_PRINTING_LIMIT)
                    {
                        Console.WriteLine("                 ___________Player A______________|___________Player B______________");
                        Console.WriteLine("{0,15}>  {1,-32}|{2,32}", "Initialize", knownBotTypes[ids[0]], knownBotTypes[ids[1]]);
                    }
                    else
                    {
                        int percent = (int)((float)round / pkCount * 10);
                        Console.SetCursorPosition(0, Console.CursorTop);
                        Console.Write("Round: " + new string('.', percent) + round);
                    }

                    try
                    {
                        var botA = (BaseBot)Activator.CreateInstance(knownBotTypes[ids[0]]);
                        var botB = (BaseBot)Activator.CreateInstance(knownBotTypes[ids[1]]);

                        if (botA != null && botB != null)
                        {
                            botA.Init();
                            botB.Init();

                            if (pkCount <= PK_PRINTING_LIMIT)
                            {
                                Console.WriteLine("{0,15}>  {1,-32:D4}|{2,32:D4}", "Answer", botA.Answer, botB.Answer);
                            }

                            int step = 1;
                            string resultA = string.Empty;
                            string resultB = string.Empty;
                            bool guessing = true;
                            while (guessing)
                            {
                                int numA = 0, numB;

                                try
                                {
                                    numA = botA.Guess(resultA);
                                    resultA = botB.Judge(numA);

                                    numB = botB.Guess(resultB);
                                    resultB = botA.Judge(numB);

                                    if (pkCount <= PK_PRINTING_LIMIT)
                                    {
                                        string infoA, infoB;
                                        if (resultA == MASKOK)
                                        {
                                            infoA = string.Format("{0:D4} [WIN, Elapsed {1}ms]", numA, botA.GuessingElapsed.ElapsedMilliseconds);
                                        }
                                        else
                                        {
                                            infoA = string.Format("{0:D4} [{1}]", numA, resultA);
                                        }

                                        if (resultB == MASKOK)
                                        {
                                            infoB = string.Format("[WIN, Elapsed {1}ms] {0:D4}", numB, botB.GuessingElapsed.ElapsedMilliseconds);
                                        }
                                        else
                                        {
                                            infoB = string.Format("[{1}] {0:D4}", numB, resultB);
                                        }

                                        Console.WriteLine("{0,15}>  {1,-32}|{2,32}", string.Format("Guess[{0:D3}]", step++), infoA, infoB);
                                    }

                                    if (resultA == MASKOK && resultB == MASKOK)
                                    {
                                        if (pkCount <= PK_PRINTING_LIMIT)
                                        {
                                            Console.WriteLine("                 ___________________________________________________________________");
                                            Console.WriteLine("{0,15}>  We got a draw!\n", "Draw");
                                        }
                                        draws++;
                                        guessing = false;
                                    }
                                    else if (resultA == MASKOK || resultB == MASKOK)
                                    {
                                        string role = "";
                                        if (resultA == MASKOK)
                                        {
                                            role = "A";
                                            winsA.Add(new long[] { botA.GuessingElapsed.ElapsedMilliseconds, botB.JudgeCount });
                                        }
                                        else
                                        {
                                            role = "B";
                                            winsB.Add(new long[] { botB.GuessingElapsed.ElapsedMilliseconds, botA.JudgeCount });
                                        }

                                        if (pkCount <= PK_PRINTING_LIMIT)
                                        {
                                            Console.WriteLine("                 ___________________________________________________________________");
                                            Console.WriteLine("{0,15}>  The winner is {1}\n", "Done", role);
                                        }
                                        guessing = false;
                                    }
                                    else if (botA.JudgeCount >= MAX_GUESS_COUNT)
                                    {
                                        if (pkCount <= PK_PRINTING_LIMIT)
                                        {
                                            Console.WriteLine("                 ___________________________________________________________________");
                                            Console.WriteLine("{0,15}>  {1}\n", "Done", "Both failed.");
                                        }
                                        guessing = false;
                                    }

                                }
                                catch (BizException ex)
                                {
                                    Console.WriteLine("                 ___________________________________________________________________");
                                    Console.ForegroundColor = ConsoleColor.Red;
                                    Console.WriteLine("{0,15}>  The bot[{1}] got an error:", "Error:", ex.Source);
                                    Console.ResetColor();
                                    Console.WriteLine("\t\t\t{0}", ex.Message);
                                    round = int.MaxValue;
                                    break;
                                }


                                //Thread.Sleep(200);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Error: {0}", ex.Message);
                        Console.ResetColor();
                        break;
                    }
                }

                Console.WriteLine();
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("                       ___A___|___B___");
                Console.WriteLine("{0,20}>  {1,-7}|{2,7}", "Wins", winsA.Count, winsB.Count);
                Console.WriteLine("{0,20}>  {1,-7:f3}|{2,7:f3}", "Guess count in Avg."
                    , winsA.Count > 0 ? (float)winsA.Sum((x) => { return x[1]; }) / winsA.Count : 0
                    , winsB.Count > 0 ? (float)winsB.Sum((x) => { return x[1]; }) / winsB.Count : 0);
                Console.WriteLine("{0,20}>  {1,-7:f3}|{2,7:f3}", "Elapsed ms in Avg."
                    , winsA.Count > 0 ? (float)winsA.Sum((x) => { return x[0]; }) / winsA.Count : 0
                    , winsB.Count > 0 ? (float)winsB.Sum((x) => { return x[0]; }) / winsB.Count : 0);
                if (draws > 0)
                {
                    Console.WriteLine("{0,20}>  {1,8}", "Draws", draws);
                }
                Console.ForegroundColor = ConsoleColor.DarkYellow;
                Console.WriteLine("{0,20}>  {1}", "A =", knownBotTypes[ids[0]]);
                Console.WriteLine("{0,20}>  {1}", "B =", knownBotTypes[ids[1]]);
                Console.ResetColor();
            }

            Console.WriteLine();
            Console.WriteLine("Press any key to start next round or press Backspace to get the main menu...");
            if (Console.ReadKey().Key != ConsoleKey.Backspace)
            {
                StartPK();
            }
        }


        static Program()
        {
            Assembly asm = Assembly.GetExecutingAssembly();
            var types = asm.DefinedTypes;
            knownBotTypes = types.Where((x) =>
            {
                return x.BaseType != null && x.BaseType.Equals(typeof(BaseBot));
            }).OrderBy((x) => { return x.Name; }).ToList();
        }
    }
}
