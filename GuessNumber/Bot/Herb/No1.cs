﻿using System;
using System.Collections.Generic;

namespace GuessNumber.Bot.Herb
{
    public class No1 : BaseBot
    {
        private int lastResult = 0;

        private List<int> maybeList = new List<int>();

        protected override int doGuess(string judgeResult)
        {
            if (lastResult == 0)
            {
                for (int n = 123; n <= 9876; n++)
                {
                    if (IsOk(n))
                    {
                        maybeList.Add(n);
                    }
                }
            }
            else
            {
                List<int> tempList = new List<int>();
                for (int i = 0; i < maybeList.Count; i++)
                {
                    if (Judge(maybeList[i], judgeResult))
                    {
                        tempList.Add(maybeList[i]);
                    }
                }
                maybeList = tempList;
            }

            return lastResult = maybeList[0];
        }

        private bool IsOk(int n)
        {
            List<int> targetNums = new List<int>(4);
            int a = n / 1000;
            int b = (n - 1000 * a) / 100;
            int c = (n - 1000 * a - 100 * b) / 10;
            int d = n - 1000 * a - 100 * b - 10 * c;
            targetNums.Add(a);

            if (targetNums.Contains(b)) { return false; }
            targetNums.Add(b);

            if (targetNums.Contains(c)) { return false; }
            targetNums.Add(c);

            if (targetNums.Contains(d)) { return false; }
            targetNums.Add(d);

            return true;
        }

        private bool Judge(int maybeNumber, string judgeResult)
        {
            List<int> targetNums = new List<int>(4);
            int a = lastResult / 1000;
            int b = (lastResult - 1000 * a) / 100;
            int c = (lastResult - 1000 * a - 100 * b) / 10;
            int d = lastResult - 1000 * a - 100 * b - 10 * c;
            targetNums.Add(a);
            targetNums.Add(b);
            targetNums.Add(c);
            targetNums.Add(d);

            List<int> nums = new List<int>(4);
            int e = maybeNumber / 1000;
            int f = (maybeNumber - 1000 * e) / 100;
            int g = (maybeNumber - 1000 * e - 100 * f) / 10;
            int h = maybeNumber - 1000 * e - 100 * f - 10 * g;
            nums.Add(e);
            nums.Add(f);
            nums.Add(g);
            nums.Add(h);

            int countA = (targetNums[0] == e ? 1 : 0)
                        + (targetNums[1] == f ? 1 : 0)
                        + (targetNums[2] == g ? 1 : 0)
                        + (targetNums[3] == h ? 1 : 0);

            int countB = 0;
            targetNums.ForEach(x =>
            {
                if (nums.Contains(x) && targetNums.IndexOf(x) != nums.IndexOf(x)) countB++;
            });

            return String.Format("{0}A{1}B", countA, countB).Equals(judgeResult);
        }
    }
}