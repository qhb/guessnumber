﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GuessNumber.Bot
{
    class _Foo : BaseBot
    {
        int n = 123;

        protected override int doGuess(string judgeResult)
        {
            do
            {
                n++;
            } while (!IsOk());

            return n;
        }

        private bool IsOk()
        {
            List<int> targetNums = new List<int>(4);
            int a = n / 1000;
            int b = (n - 1000 * a) / 100;
            int c = (n - 1000 * a - 100 * b) / 10;
            int d = n - 1000 * a - 100 * b - 10 * c;
            targetNums.Add(a);

            if (targetNums.Contains(b)) { return false; }
            targetNums.Add(b);

            if (targetNums.Contains(c)) { return false; }
            targetNums.Add(c);

            if (targetNums.Contains(d)) { return false; }
            targetNums.Add(d);

            return true;
        }
    }
}
